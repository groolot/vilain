[![Stories in Ready](https://badge.waffle.io/groolot/vilain.png?label=ready&title=Ready)](https://waffle.io/groolot/vilain)
vilain::
========
[![Build Status](https://travis-ci.org/groolot/vilain.svg?branch=master)](https://travis-ci.org/groolot/vilain)

install
=======
1. clone the repository ```git clone https://github.com/groolot/vilain.git```
2. ```cd vilain```
3. lauch install script: ```./install_prerequisites```
4. if errors reported, feel free to write an issue

credits 
=======
Lead developer:
- Gregory David

GUI:
- François-Xavier Lebret (design)
- Ronan Legardinier (research)

Integrations:
- Gregory David (Travis CI)
- Aurelien Roux (Raspberry Pi)

Packaging:
- Florian Thierry (Debian GNU/Linux)

Documentation:
- Ronan Legardinier
- Gregory David

Translations:
- Jean-Emmanuel Doucet (fr)
