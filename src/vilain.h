//---------- System
#include <GLFW/glfw3.h>

//---------- i18n
#include <libintl.h>
#define _(String) gettext(String)
#define _N(String) String

//---------- OSC
#include "ofxOscParameterSync.h"

//---------- Addons
#include "ofxImageSequence.h"

//---------- Globals
#include "vilainConstants.h"
